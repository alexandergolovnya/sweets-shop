package com.innopolis.sweetsshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SweetsShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(SweetsShopApplication.class, args);
	}

}
