package com.innopolis.sweetsshop.repository;

import com.innopolis.sweetsshop.model.Sweets;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SweetsRepository extends CrudRepository<Sweets, UUID> {
}
