package com.innopolis.sweetsshop.controller;

import com.innopolis.sweetsshop.model.Sweets;
import com.innopolis.sweetsshop.service.SweetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/sweets")
public class SweetsController {
    
    @Autowired
    private SweetsService sweetsService;
    
    @GetMapping
    public List<Sweets> getAllSweets() {
        return sweetsService.getAll();
    }
    
    @GetMapping("/{id}")
    public Sweets getById(@PathVariable UUID id) {
        return sweetsService.getById(id);
    }
    
    @PostMapping
    public Sweets createSweets(@RequestBody Sweets sweets) {
        return sweetsService.createSweets(sweets);
    }
    
    @PutMapping("/{id}")
    public Sweets updateSweets(@PathVariable UUID id, @RequestBody Sweets sweets) {
        return sweetsService.updateSweets(id, sweets);
    }
    
    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        sweetsService.delete(id);
    }
}
