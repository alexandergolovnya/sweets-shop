package com.innopolis.sweetsshop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping({""})
public class FrontendController {
    
    @GetMapping
    public String startPage(Model model, @RequestParam(value = "name", defaultValue = "World") String name) {
        model.addAttribute("name", name);
        return "index";
    }
    
    @GetMapping("/js")
    public String startPageJS() {
        return "js-example";
    }
}
