package com.innopolis.sweetsshop.controller;

import com.innopolis.sweetsshop.model.Greeting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    
    private Integer counter = 1;
    
    @GetMapping("/greeting")
    public Greeting createGreeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter++, String.format("Hello, %s!", name));
    }
}
