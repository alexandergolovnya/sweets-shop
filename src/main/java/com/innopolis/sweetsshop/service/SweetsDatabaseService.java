package com.innopolis.sweetsshop.service;

import com.innopolis.sweetsshop.model.Sweets;
import com.innopolis.sweetsshop.repository.SweetsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Slf4j
@Service
@ConditionalOnProperty(value = "sweets-service.type", havingValue = "database")
public class SweetsDatabaseService implements SweetsService {
    
    private final SweetsRepository sweetsRepository;
    
    public SweetsDatabaseService(SweetsRepository sweetsRepository) {
        this.sweetsRepository = sweetsRepository;
        log.info("{} created", this.getClass().getSimpleName());
    }
    
    @PostConstruct
    public void init() {
        for (int i = 1; i <= 10; ++i) {
            Sweets sweets = new Sweets(UUID.randomUUID(), "Sweets " + i, "Test", "100");
            sweetsRepository.save(sweets);
        }
    }
    
    @Override
    public List<Sweets> getAll() {
        ArrayList<Sweets> sweetsList = new ArrayList<>();
        Iterable<Sweets> sweetsIterable = sweetsRepository.findAll();
        sweetsIterable.forEach(sweetsList::add);
        return sweetsList;
    }
    
    @Override
    public Sweets getById(UUID id) {
        return sweetsRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Sweets with id %s not found", id)));
    }
    
    @Override
    public Sweets createSweets(Sweets sweets) {
        sweets.setId(UUID.randomUUID());
        return sweetsRepository.save(sweets);
    }
    
    @Override
    public Sweets updateSweets(UUID id, Sweets sweets) {
        Sweets sweetsFromDatabase = getById(id);
        sweetsFromDatabase.setTitle(sweets.getTitle());
        sweetsFromDatabase.setType(sweets.getType());
        sweetsFromDatabase.setCalories(sweets.getCalories());
        
        return sweetsRepository.save(sweetsFromDatabase);
    }
    
    @Override
    public void delete(UUID id) {
        sweetsRepository.deleteById(id);
    }
}
