package com.innopolis.sweetsshop.service;

import com.innopolis.sweetsshop.model.Sweets;

import java.util.List;
import java.util.UUID;

public interface SweetsService {
    List<Sweets> getAll();
    Sweets getById(UUID id);
    Sweets createSweets(Sweets sweets);
    Sweets updateSweets(UUID id, Sweets sweets);
    void delete(UUID id);
}
