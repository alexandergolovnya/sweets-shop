package com.innopolis.sweetsshop.service;

import com.innopolis.sweetsshop.model.Sweets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.lang.String.format;

@Slf4j
@Service
@ConditionalOnProperty(value = "sweets-service.type", havingValue = "in-memory")
public class SweetsInMemoryService implements SweetsService {
    
    private final List<Sweets> database = new ArrayList<>();
    
    public SweetsInMemoryService() {
        log.info("{} created", this.getClass().getSimpleName());
    }
    
    @PostConstruct
    public void init() {
        for (int i = 1; i <= 10; ++i) {
            Sweets sweets = new Sweets(UUID.randomUUID(), "Sweets " + i, "Test", "100");
            database.add(sweets);
        }
    }
    
    @Override
    public List<Sweets> getAll() {
        return database;
    }
    
    @Override
    public Sweets getById(UUID id) {
        return database.stream()
                .filter(sweets -> sweets.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Sweets with id %s not found", id)));
    }
    
    @Override
    public Sweets createSweets(Sweets sweets) {
        sweets.setId(UUID.randomUUID());
        database.add(sweets);
        return sweets;
    }
    
    @Override
    public Sweets updateSweets(UUID id, Sweets sweets) {
        Sweets sweetsFromDatabase = getById(id);
        sweetsFromDatabase.setTitle(sweets.getTitle());
        sweetsFromDatabase.setType(sweets.getType());
        sweetsFromDatabase.setCalories(sweets.getCalories());
        return sweetsFromDatabase;
    }
    
    @Override
    public void delete(UUID id) {
        Sweets sweets = getById(id);
        database.remove(sweets);
    }
}
