const HttpClient = function() {
    this.request = function(url, requestType, body, callbackFunction) {
        const httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState == 4 && httpRequest.status == 200)
                callbackFunction(httpRequest.responseText);
        }

        httpRequest.open(requestType, url, true);
        httpRequest.send(body);
    }
}

let greeting = {
    "id": "",
    "content": "",
}

const client = new HttpClient();
client.request('http://localhost:8090/greeting', "GET", null,function(response) {
    // do something with response
    greeting = response;
    console.log("Greeting: " + greeting);
    const greetingContent = document.getElementById("greetingContent");
    greetingContent.innerHTML = greeting;
});