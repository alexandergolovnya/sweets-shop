CREATE TABLE sweets (
    id UUID PRIMARY KEY,
    title VARCHAR(255),
    type VARCHAR(255),
    calories VARCHAR(255)
)